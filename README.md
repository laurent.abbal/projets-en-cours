**Mon Oral**
* Pratique de l'oral au primaire et au secondaire, préparation aux épreuves orales de collège et de lycée & création de commentaires audio pour les élèves.
* Développement : [forge.aeif.fr/mon-oral](https://forge.aeif.fr/mon-oral)
* Site : [www.mon-oral.net](https://www.mon-oral.net)

**Nuit du c0de**
* La Nuit du c0de est un marathon de programmation durant lequel les élèves, par équipes de deux ou trois, ont 6h (ou 4h pour le cycle 3) pour coder un jeu avec Scratch ou Python. Du CM1 à la Terminale.
* Développement : [forge.aeif.fr/nuit-du-code](https://forge.aeif.fr/nuit-du-code)
* Site : [www.nuitducode.net/](https://www.nuitducode.net/)

**Atelier Numérique**
* Les ateliers permettent d'avoir, côte à côte, un document de travail (cours, tutoriel, exercices...) et un environnement de création (Scratch, Geogebra, Basthon, BlocksCAD...). Formats possibles pour les documents de travail : vidéo, PDF, page web...
* Développement : [forge.aeif.fr/atelier-numerique](https://forge.aeif.fr/atelier-numerique)
* Site : [www.atelier-numerique.net](https://www.atelier-numerique.net)

**Code Puzzle**
* Générateur et gestionnaire de puzzles de "Parsons".
* Développement : [forge.aeif.fr/code-puzzle](https://forge.aeif.fr/code-puzzle)
* Site : [www.codepuzzle.io/](https://www.codepuzzle.io/)

**Edupyter**
* Environnement de développement pour l'enseignement et l'apprentissage de Python. Peut être installé sur un disque dur ou une clé USB sans droits administrateur. Facile à déployer, à mettre à jour et à enrichir.
* Développement : [forge.aeif.fr/edupyter](https://forge.aeif.fr/edupyter)
* Site : [www.edupyter.net](https://www.edupyter.net)

**Pyxel Studio**
* Studio de développement pour Pyxel.
* Développement : [forge.aeif.fr/pyxel-styudio](https://forge.aeif.fr/pyxel-studio)
* Site : [www.pyxelstudio.net](https://www.pyxelstudio.net)

**Dozo**
* Encadrez l'activité de navigation de vos élèves et aidez-les à se concentrer sur leur tâche. Choisissez le nombre de sites qu'ils peuvent ouvrir simultanément, les sites qu'ils peuvent visiter ou ceux qu'ils ne doivent pas utiliser.
* Développement : [forge.aeif.fr/dozo](https://forge.aeif.fr/dozo)
* Site : [www.dozo.app](https://www.dozo.app)

**Microlab**
* Le but du projet microlab.cc est de développer pour le collège et le lycée du matériel de laboratoire "open source" en kit facile à monter et diffuser dans les établissements.
* Développement : [forge.aeif.fr/microlab](https://forge.aeif.fr/microlab)
* Site : [www.microlab.cc](https://www.microlab.cc)

